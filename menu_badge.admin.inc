<?php

/**
 * Menu callback for admin/build/menu_badge
 */
function menu_badge_setup() {
  $result = db_query("SELECT * FROM {menu_custom} ORDER BY title");
  $content = array();
  while ($menu = db_fetch_array($result)) {
    $content[] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => $menu['title'],
      '#description' => $menu['description'],
      'menu' => array('#value' => _menu_badge_setup_loadmenu($menu['menu_name'])),
    );
  }
  return drupal_render($content);
}

/**
 * Helper function for load single menu admin panel.
 */
function _menu_badge_setup_loadmenu($menu) {
  // Load tree
  $sql = "SELECT m.load_functions, m.to_arg_functions, m.access_callback, m.access_arguments, m.page_callback, m.page_arguments, m.title, m.title_callback, m.title_arguments, m.type, m.description, ml.*
    FROM {menu_links} ml LEFT JOIN {menu_router} m ON m.path = ml.router_path
    WHERE ml.menu_name = '%s'
    ORDER BY p1 ASC, p2 ASC, p3 ASC, p4 ASC, p5 ASC, p6 ASC, p7 ASC, p8 ASC, p9 ASC";
  $result = db_query($sql, $menu);
  $tree = menu_tree_data($result);
  $node_links = array();
  menu_tree_collect_node_links($tree, $node_links); krumo($tree);
  
  // Generate list of menu items
  $result = array();
  uasort($tree, "_menu_badge_setup_genlist_element_sort");
  foreach ($tree as $item)
    _menu_badge_setup_genlist($result, $item);
  
  // Render
  if (count($result) == 0) {
    return '<em>' . t('There are no menu items yet.') . '</em>';
  }
  return theme('table', array(t('Path'), ''), $result);
}

/**
 * Helper function for generate list of menu items. Recursive.
 */
function _menu_badge_setup_genlist(&$result, $item) {
  if($item['link']['type'] != MENU_NORMAL_ITEM)
    return FALSE;
  
  $result[] = array(
    str_repeat('&nbsp;&nbsp;&nbsp; ', $item['link']['depth'] - 1)
      . check_plain($item['link']['link_path']),
    ($item['link']['page_callback'] == 'views_page') ? l(t('Add Views badge'), 'admin/build/menu_badge/edit/view/' . $item['link']['mlid']) : '',
  );
  
  // Recursive
  if($item['below']) {
    uasort($item['below'], "_menu_badge_setup_genlist_element_sort");
    foreach ($item['below'] as $child_item)
      _menu_badge_setup_genlist($result, $child_item);
  }
}

/**
 * Function used by uasort to sort structured arrays by weight.
 */
function _menu_badge_setup_genlist_element_sort($a, $b) {
  $a = (array)$a['link'] + array('weight' => 0, 'title' => '');
  $b = (array)$b['link'] + array('weight' => 0, 'title' => '');
  return $a['weight'] < $b['weight'] ? -1 : ($a['weight'] > $b['weight'] ? 1 : ($a['title'] < $b['title'] ? -1 : 1));
}





